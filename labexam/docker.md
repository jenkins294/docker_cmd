# docker

## installation

```bash

# update the apt cache
> sudo apt-get update

# install pre-requisites
> sudo apt-get install ca-certificates curl gnupg lsb-release

# download the key for getting the docker apt repository
> sudo mkdir -p /etc/apt/keyrings
> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# add an entry in apt repository to install docker
> echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# update the apt cache
> sudo apt-get update

# install docker
> sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

```

## docker images

- file which contains set of instructions for creating a container

```bash

# get the list of images downloaded on your machine
> docker image ls

# to delete an image downloaded on your machine
> docker image rm <image name>

# download the image on your machine
> docker image pull <name>
> docker image pull hello-world

# get details or metadata of selected image
> docker image inspect <image name>

```

## docker container

```bash

# get the list of running containers
> docker container ls

# get the list of all (running and not running) containers
> docker container ls -a

# create a container
> docker container create <image name>
> docker container create hello-world

# start the container
# when the container starts, it executes the program it was meant to execute
# when or if the program exits (for any reason), the container stops / exits
> docker container start <container name or container id>

# create and start the container
> docker container run <image name>

```
